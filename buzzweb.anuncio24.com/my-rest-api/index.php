<?php

//Register an autoloader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(array('./models/'))->register();


$di = new \Phalcon\DI\FactoryDefault();

// Настройка сервиса базы данных
$di->set('db', function(){
    return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
        "host" => "localhost",
        "username" => "root",
        "password" => "proval",
        "dbname" => "buzzweb"
    ));
});

$app = new \Phalcon\Mvc\Micro($di);

// тут определяются роуты


// получение списка дочерних регионов указанного родительского региона
$app->get('/api/regions/{id:[0-9]+}', function($id) use ($app) {

    $phql = "SELECT * FROM region WHERE parent_region_id = :id: ORDER BY region_id";
    $regions = $app->modelsManager->executeQuery($phql, array('id' => $id));


    $data = array();
    foreach( $regions as $region){
        $data[] = array(
        'region_id' => $region->region_id,
        'region_name' => $region->region_name,
                        );
    }
    echo json_encode($data);
});

// получение списка дочерних регионов указанного родительского региона
$app->get('/api/peoplefrom/{id:[0-9]+}', function($id) use ($app) {

    $phql = "SELECT * FROM people WHERE region_id = :id: ORDER BY people_id";
    $peoples = $app->modelsManager->executeQuery($phql, array('id' => $id));


    $data = array();
    foreach( $peoples as $people){
        $data[] = array(
        'people_id' => $people->people_id,
        'people_name' => $people->people_name,
                        );
    }
    echo json_encode($data);
});

// получение человека по id
$app->get('/api/peopleid/{id:[0-9]+}', function($id) use ($app) {

    $phql = "SELECT * FROM people WHERE people_id = :id:";
    $people = $app->modelsManager->executeQuery($phql, array(
        'id' => $id
    ))->getFirst();

    //Create a response
    $response = new Phalcon\Http\Response();

    if ($people == false) {
        $response->setJsonContent(array('status' => 'NOT-FOUND'));
    } else {
        $response->setJsonContent(array(
            'status' => 'FOUND',
            'data' => array(
                'region_id' => $people->region_id,
                'region_name' => $people->people_name
            )
        ));
    }

    return $response;
});



$app->handle();
