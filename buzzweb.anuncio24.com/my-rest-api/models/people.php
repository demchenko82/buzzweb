<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

    class people extends Model
    {

        public function validation()
        {
            if ($this->region_id < 0) {
            $this->appendMessage(new Message("The Region is integer"));
            }

            // Проверяет, были ли получены какие-либо сообщения при валидации
            if ($this->validationHasFailed() == true) {
            return false;
            }
        }
    }
